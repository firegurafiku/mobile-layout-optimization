Mobile phone keyboard optimization for T9-like input
====================================================

(Nothing yet.)

## Reproducing the experiment

After you have cloned the repository, compile all Haskell programs in it. Cabal
will resolve and download all dependencies needed.

    $ cabal build

Then, download source data from Google N-Grams service and process them into
a much smaller dataset. Because the original data is about 4.5 GB in compressed
form, we would uncompress it on the fly. Aslo, parameter '1950' mean the minimum
year to consider, '5000' is the number of the most frequent words to extract,
third parameter specifies a set of allowed characters. Note that dataset
processing may take a while (it took about 1 hour on my laptop).

    $ wget http://storage.googleapis.com/books/ngrams/books/googlebooks-eng-all-1gram-20120701-{a..z}.gz
    $ zcat googlebooks-eng-all-1gram-20120701-{a..z}.gz |
          ./PrepareGoogleDataset 1950 5000 "$(echo {a..z})" > dataset.txt

After all preparations is done, run actuall simulation process.

    $ ./KeyboardLayout "$(echo {a..z})" dataset.txt logfile.log

